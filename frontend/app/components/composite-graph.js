import Ember from 'ember';

export default Ember.Component.extend({
	sortOptions: {"User handle": 'label', Value: 'value'},
	sortBy: 'label',
	title: null,
	data: [],
	yAxisLabel: '',
	color: 'rbg(60, 60, 60)'
});

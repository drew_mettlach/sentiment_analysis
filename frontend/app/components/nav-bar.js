import Ember from 'ember';

export default Ember.Component.extend({
	activeTab: null,
	didInsertElement: function() {
		this.$('.nav-button').each(function(i, e) {
			if($(e).hasClass('active')) {
				$(e).parent().addClass('active');
			}
		});

		this.$('.nav-button').click(function() {
			$('.nav-button').each(function(i, e) {
				$(e).parent().removeClass('active');
			});
			
			$(this).parent().addClass('active');
		});
	}
});

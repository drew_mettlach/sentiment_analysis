import Ember from 'ember';

export default Ember.Component.extend({
	orderedUsers: Ember.computed.sort('users', 'sortField'),
	sortField: ['handle']
});

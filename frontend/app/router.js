import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('favorites');
  this.route('home', {path: '/not-home'});
  this.route('twitter-user', {path: '/twitter-user/:user_id'});
  this.route('twitter-users');
  this.route('composite');
});

export default Router;

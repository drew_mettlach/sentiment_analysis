import DS from 'ember-data';

export default DS.Model.extend({
	twitterId: DS.attr('number'),
	handle: DS.attr('string'),
	tweetCount: DS.attr('number'),
	percentPositive: DS.attr('number'),
	percentNegative: DS.attr('number'),
	percentNeutral: DS.attr('number'),

	// Custom colors for some handles
	chartColors: {
		'realDonaldTrump': 'rgb(233, 242, 64)',
		'KermitTheFrog': 'rgb(0, 255, 0)'
	},

	chartColor: function() {
		var handle = this.get('handle'),
			color = this.chartColors[handle];

		// If we don't have a custom color defined, use a random color
		if(typeof color === "undefined") {
			var r = this.getRandInt(0, 256);
			var g = this.getRandInt(0, 256);
			var b = this.getRandInt(0, 256);

			color = 'rgb('+r+', '+g+', '+b+')';
		}
		return color;
	}.property('handle'),
	
	// TODO: might be better in a mixin
	getRandInt: function(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min)) + min;
	}
});

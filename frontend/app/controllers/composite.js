import Ember from 'ember';

export default Ember.Controller.extend({
	percentPositiveColor: 'rgb(66, 244, 69)',
	percentNegativeColor: 'rgb(244, 66, 69)',
	percentNeutralColor: 'rgb(0, 0, 0)',
	// Build up our chart data. Will be cached by ember
	// and should rarely be updated
	graphTypes: function() {
		var retVal = [
			{type: "Positive", label: "Positive (%)", data: this.get('positiveData'), color: this.get('percentPositiveColor')},
			{type: "Negative", label: "Negative (%)", data: this.get('negativeData'), color: this.get('percentNegativeColor')},
			{type: "Neutral", label: "Neutral: (%)", data: this.get('neutralData'), color: this.get('percentNeutralColor')}
		];
		return retVal;
	}.property('positiveData','negativeData','neutralData'),
	positiveData: function() {
		return this.userData('percentPositive');
	}.property('model.@each.percentPositive', 'model.@each.handle'),
	negativeData: function() {
		return this.userData('percentNegative');
	}.property('model.@each.percentNegative', 'model.@each.handle'),
	neutralData: function() {
		return this.userData('percentNeutral');
	}.property('model.@each.percentNeutral','model.@each.handle'),

	userData: function(valueKey) {
		var users = this.get('model'),
			data = [];

		users.forEach(function(user) {
			var handle = user.get('handle'),
				value = user.get(valueKey),
				datum = 
				{
					"label": handle,
					"value": value
				};

			data.push(datum);
		});

		return data;
	}
});

import Ember from 'ember';

export default Ember.Controller.extend({
	width: 400,
	height: 400,

	dataArray: function() {
		var model = this.get('model'),
			dataArray = 
			[
				{
					'value': model.get('percentPositive'), 
					'label': 'Percent Positive'
				},
				{
					'value': model.get('percentNegative'),
					'label': 'Percent Negative'
				},
				{
					'value': model.get('percentNeutral'),
					'label': 'Percent Neutral'
				}
			];

		return dataArray;
	}.property('model.percentPositive,model.percentNegative,model.percentNeutral'),
});

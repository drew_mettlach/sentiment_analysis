require 'test_helper'

class TwitterUserTest < ActiveSupport::TestCase
  test "valid twitter user saves" do
    user = TwitterUser.first

    assert user.save
  end

  test "twitter user should not save without twitter id" do
  	user = TwitterUser.first
   	user.twitter_id = nil

  	assert_not user.save
  end

  test "twitter user should not save without handle" do
  	user = TwitterUser.first
  	user.handle = nil

  	assert_not user.save
  end

  test "twitter user should not save with blank handle" do
  	user = TwitterUser.first
  	user.handle = ''

  	assert_not user.save
  end

  test "twitter user should not save with existing handle" do
  	user1 = TwitterUser.first
  	user2 = TwitterUser.last

  	user1.handle = user2.handle

  	assert_not user1.save
  end

  test "twitter user should not save with existing twitter id" do
  	user1 = TwitterUser.first
  	user2 = TwitterUser.last

  	user1.twitter_id = user2.twitter_id

  	assert_not user1.save
  end
end

require 'yaml'

namespace :twitter_api do
	# Expects a file in config called twitter_credentials.yml
	# that has a valid entry for 'twitter_auth_token'
	config_file = 'config/twitter_credentials.yml'
	config = YAML.load_file(config_file)
	twitter_auth_token = config['twitter_auth_token']
	twitter_base_url = 'https://api.twitter.com/1.1'
	
	headers = {"Authorization" => "Bearer #{twitter_auth_token}"}
	handles = [
		'realDonaldTrump',
		'BarackObama',
		'MichelleObama',
		'TheEllenShow',
		'KermitTheFrog',
		'billburr',
		'BillNye',
		'TheRock',
		'RichardDawkins',
		'rickygervais',
		'smokey_bear',
		'incredibleviews'
	]

	task :test_config => :environment do
		puts headers
	end

	desc "Pulls user data from twitter for any non-existing handles and saves to DB"
	task :pull_new_users => :environment do
		cs_handles = handles.join ','
		url = "#{twitter_base_url}/users/lookup.json?screen_name=#{cs_handles}"
		user_data = HTTParty.get url, headers: headers
		puts "Got #{user_data.length} users from twitter"
		for user_datum in user_data
			handle = user_datum['screen_name']
			tu = TwitterUser.find_by handle: handle

			if tu.nil?
				TwitterUser.create handle: handle, twitter_user_id: user_datum['id']
				puts "Saved new twitter user #{handle}"
			elsif tu.twitter_user_id != user_datum['id']
				tu.update_attributes twitter_user_id: user_datum['id']
				puts "Updated id for user #{handle}"
			else
				puts "Nothing to change for user #{handle}"
			end
		end
	end

	desc "Pulls tweet data for all saved handles and saves to DB"
	task :pull_tweets, [:historical] => :environment do |t, args|
		url = "#{twitter_base_url}/statuses/user_timeline.json?"

		historical = args[:historical]
		if historical == 'true'
			historical = true
		elsif historical == 'false'
			historical = false
		else
			puts "Invalid parameter historical: #{historical}"
			exit
		end

		for handle in handles
			tu = TwitterUser.find_by handle: handle
			if tu.nil?
				puts "#{handle} not found"
				next
			end

			puts "Fetching tweets for #{handle}"

			count = 0
			while(true)
				cur_url = "#{url}screen_name=#{handle}&count=200"

				if tu.max_id and historical
					cur_url = "#{cur_url}&max_id=#{tu.max_id-1}"
				elsif tu.since_id and not historical
					cur_url = "#{cur_url}&since_id=#{tu.since_id}"
				end

				puts "Requesting #{cur_url}"
				data = HTTParty.get cur_url, headers: headers
				ids = []
				for tweet in data
					count += 1
					tweet_id = tweet['id']
					text = tweet['text']
					twitter_user_id = tu.id
					date = tweet['created_at']
					retweet = tweet['retweeted_status'].nil? ? false : true

					puts "#{count} #{tweet_id} #{twitter_user_id} #{date} #{retweet}"

					Tweet.create(
						tweet_id: tweet_id, 
						body: text, 
						twitter_user_id: twitter_user_id, 
						handle: handle, 
						tweet_date: date,
						retweet: retweet
					)

					ids.append tweet_id
				end

				if not ids.empty?
					puts "Got #{ids.length} tweets"
					if historical
						max_id = ids.min
						if not tu.since_id.nil?
							since_id = [ids.max, tu.since_id].max
						else
							since_id = ids.max
						end
					else
						since_id = ids.max
						if not tu.max_id.nil?
							max_id = [ids.min, tu.max_id].min
						else
							max_id = ids.min
						end
					end

					tu.update_attributes max_id: max_id, since_id: since_id
				else
					puts "Done"
					break
				end

				tu.last_import_date = Time.now
			end
		end
	end
end
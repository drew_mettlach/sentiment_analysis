namespace :sentiment_analysis do

	desc "Calculates sentiment analysis scores for each handle's tweets"
	task :cache_scores => :environment do
		analyzer = Sentimental.new
		analyzer.load_defaults
		analyzer.threshold = 0.1

		t_users = TwitterUser.all
		puts "Calculating scores for #{t_users.length} users"
		for t_user in t_users
			handle = t_user.handle

			tweets = t_user.tweets
			print_exit "No tweets for #{handle}" if tweets.empty?

			puts "Calculating sentiment scores for #{handle}"
			count = 0
			for tweet in tweets
				score = analyzer.score tweet.body
				tweet.update_attributes sentiment_score: score
				count += 1
				if count == 0 or count%100 == 0
					print '.'
				end
			end
			puts "Done"
		end
	end

	desc "Calculates the seniment percentages for each handle"
	task :cache_percentages => :environment do
		t_users = TwitterUser.all
		for t_user in t_users
			puts "Calculating percentages for #{t_user.handle}"

			positive = negative = neutral = 0
			tweets = t_user.tweets
			count = 0
			for tweet in tweets
				if tweet.is_positive?
					positive += 1
				elsif
					tweet.is_negative?
					negative += 1
				else
					neutral += 1
				end

				count += 1
				if count == 0 or count%100 == 0
					print '.'
				end
			end

			ret = {positive: positive, negative: negative, neutral: neutral}
			den = Float(tweets.length)
			ret.each{|k,v| ret[k] = 100*(v/den)}

			t_user.update_attributes percent_positive: ret[:positive], percent_negative: ret[:negative], percent_neutral: ret[:neutral]

			puts "Done"
		end
	end

	def print_exit(message)
		puts "#{message}"
		exit
	end
end
class ChangeTwitterUserIdTypeInTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	change_column :twitter_users, :twitter_user_id, :bigint
  end
end

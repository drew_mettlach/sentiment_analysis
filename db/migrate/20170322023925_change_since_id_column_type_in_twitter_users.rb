class ChangeSinceIdColumnTypeInTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	change_column :twitter_users, :since_id, :bigint
  end
end

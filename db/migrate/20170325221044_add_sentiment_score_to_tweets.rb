class AddSentimentScoreToTweets < ActiveRecord::Migration[5.0]
  def change
  	add_column :tweets, :sentiment_score, :integer
  end
end

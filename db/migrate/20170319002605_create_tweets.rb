class CreateTweets < ActiveRecord::Migration[5.0]
	def change
		create_table :tweets do |t|
			t.integer	:tweet_id
			t.string 	:body
			t.string	:handle
			t.datetime	:tweet_date
			t.integer	:twitter_user_id
			t.timestamps
		end
	end
end

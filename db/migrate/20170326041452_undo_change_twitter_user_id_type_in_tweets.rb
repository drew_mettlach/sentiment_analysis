class UndoChangeTwitterUserIdTypeInTweets < ActiveRecord::Migration[5.0]
  def change
  	change_column :tweets, :twitter_user_id, :integer
  end
end

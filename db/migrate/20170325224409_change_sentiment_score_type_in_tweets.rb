class ChangeSentimentScoreTypeInTweets < ActiveRecord::Migration[5.0]
  def change
  	change_column :tweets, :sentiment_score, :float
  end
end

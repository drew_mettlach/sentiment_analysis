class AddSentimentPercentColumnsToTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :twitter_users, :percent_positive, :float
  	add_column :twitter_users, :percent_negative, :float
  	add_column :twitter_users, :percent_neutral, :float
  end
end

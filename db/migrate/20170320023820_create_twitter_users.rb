class CreateTwitterUsers < ActiveRecord::Migration[5.0]
	def change
		create_table :twitter_users do |t|
			t.integer :max_id
			t.integer :since_id
			t.integer :twitter_user_id
			t.string :handle
			t.timestamps
		end
	end
end

class ChangeMaxIdTypeInTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	change_column :twitter_users, :max_id, :bigint
  end
end

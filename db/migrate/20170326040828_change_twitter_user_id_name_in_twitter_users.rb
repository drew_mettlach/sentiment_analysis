class ChangeTwitterUserIdNameInTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	rename_column :twitter_users, :twitter_user_id, :twitter_id
  end
end

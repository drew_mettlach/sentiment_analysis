class AddRetweetToTweets < ActiveRecord::Migration[5.0]
  def change
  	add_column :tweets, :retweet, :boolean, default: false
  end
end

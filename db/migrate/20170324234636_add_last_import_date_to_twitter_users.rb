class AddLastImportDateToTwitterUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :twitter_users, :last_import_date, :date
  end
end

class AddIndexesToTables < ActiveRecord::Migration[5.0]
  def change
  	add_index :twitter_users, :handle, unique: true
  	add_index :twitter_users, :twitter_user_id, unique: true

  	add_index :tweets, :handle
  	add_index :tweets, :tweet_id, unique: true
  end
end

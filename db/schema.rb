# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170326165303) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "tweets", force: :cascade do |t|
    t.bigint   "tweet_id"
    t.string   "body"
    t.string   "handle"
    t.datetime "tweet_date"
    t.integer  "twitter_user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "retweet",         default: false
    t.float    "sentiment_score"
    t.index ["handle"], name: "index_tweets_on_handle", using: :btree
    t.index ["tweet_id"], name: "index_tweets_on_tweet_id", unique: true, using: :btree
  end

  create_table "twitter_users", force: :cascade do |t|
    t.bigint   "max_id"
    t.bigint   "since_id"
    t.bigint   "twitter_id"
    t.string   "handle"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.date     "last_import_date"
    t.float    "percent_positive"
    t.float    "percent_negative"
    t.float    "percent_neutral"
    t.index ["handle"], name: "index_twitter_users_on_handle", unique: true, using: :btree
    t.index ["twitter_id"], name: "index_twitter_users_on_twitter_id", unique: true, using: :btree
  end

end

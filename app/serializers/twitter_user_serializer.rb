class TwitterUserSerializer < ActiveModel::Serializer
	attributes(
		:id, 
		:twitter_id, 
		:handle, 
		:tweet_count,
		:percent_positive, 
		:percent_negative, 
		:percent_neutral
	)
end

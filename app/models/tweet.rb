class Tweet < ApplicationRecord
	validates :handle, presence: true
	validates :twitter_user_id, presence: true

	belongs_to :twitter_user

	def is_positive?
		sentiment_score > 0
	end

	def is_neutral?
		sentiment_score == 0
	end

	def is_negative?
		sentiment_score < 0
	end
end
 
class TwitterUser < ApplicationRecord
	validates :twitter_id, presence: true, uniqueness: true
	validates :handle, presence: true, uniqueness: true

	has_many :tweets

	def tweet_count
		tweets.length
	end
end
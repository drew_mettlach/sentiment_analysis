class TwitterUsersController < ApplicationController
	def index
		users = TwitterUser.all
		render json: users
	end

	def show
		user = TwitterUser.find params[:id]
		render json: user
	end
end

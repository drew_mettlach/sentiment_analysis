Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace "api" do
  	resources :twitter_users, :only => [:index, :show], defaults: {format: :json}
  end

  Rails.application.routes.draw do
    mount_ember_app :frontend, to: "/sentiment_analysis"
  end
end

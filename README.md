# README

This app is not production ready.  It was just done to practice rails and ember and to
work on an interesting project.

This is a rails api with an ember front end that performs sentiment analysis on tweets and
displays that data using graphs from ember-charts


* Ruby version - 2.4.0

* System dependencies

* Configuration
  * In order to run twitter api tasks, your twitter api token must be placed in a yaml file at
    config/twitter_credentials.yml with key twitter_auth_token

* Database creation
  * Create a new postgres db with a password.  List the owner and password in the config/database.yml file

* Database initialization
  1. Run rake db:migrate
  2. Run rake twitter_api:pull_new_users
  3. Run rake twitter_api:pull_tweets[true]
  4. Run rake sentiment_analysis:cache_scores
  5. Run rake sentiment_analysis:cache_percentages

* Services (job queues, cache servers, search engines, etc.)
  * The twitter_api:pull_tweets[false] and both sentiment_analysis tasks can be regularly run in cron 
    to keep the DB updated with the latest tweets
